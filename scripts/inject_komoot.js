function add_script(url){
    var script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', chrome.runtime.getURL(url));
    document.documentElement.appendChild(script);
}

add_script('scripts/komoot_regions.js');
