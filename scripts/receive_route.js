document.addEventListener(document.getElementById('route_inj').dataset.extid, e => {
    // if gpx then show the data on a map
    if (e.detail.type == 'gpx') {

        let map_var;
        if (typeof map !== 'undefined') {
            map_var = map;
        } else if (typeof gMap !== 'undefined') {
            map_var = gMap;
        } else if (typeof drupalSettings.mundraub_map.map !== 'undefined') {
            map_var = drupalSettings.mundraub_map.map;
        }

        if (map_var) {
            // draw gpx to map
            let optns = {
                async: true,
                marker_options: {
                    startIconUrl: false,
                    endIconUrl: false
                    }
            }
            new L.GPX(e.detail.data, optns).on('loaded', function(e) {
                map_var.fitBounds(e.target.getBounds());
            }).addTo(map_var);
        }
    }
    console.log(e.detail);
});


// this works but is a little bit slower
let gpx_script = document.createElement('script');
gpx_script.src = "https://cdnjs.cloudflare.com/ajax/libs/leaflet-gpx/1.5.2/gpx.min.js";
gpx_script.setAttribute('type', 'text/javascript');
document.documentElement.appendChild(gpx_script);

setTimeout(() => {
document.dispatchEvent(new CustomEvent(document.getElementById('route_inj').dataset.extid,{detail:{type:'status',data:'loaded'}}));
}, 1000);
