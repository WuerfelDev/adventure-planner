function load_leaflet(callback){
    let script = document.createElement('script');
    document.getElementsByTagName('head')[0].appendChild(script);
    script.onload = callback;
    script.src = "https://unpkg.com/leaflet@latest/dist/leaflet.js";
}



function komoot_regions(){

    // Info Text
    let info = document.createElement("h3");
    info.innerText = "You unlocked "+kmtBoot.getProps().packages.length+" packages"+(kmtBoot.getProps().freeProducts.length>0?" and got "+kmtBoot.getProps().freeProducts.length+" available for free":':');
    info.classList.add('tw-text-xl');

    // Map + its container
    // using same classes as komoot uses on its map
    let better_map_element = document.createElement("div");
    better_map_element.id = "better_map";

    let map_container = document.createElement("div");
    map_container.classList.add("c-inline-map__container","tw-mb-3","md:tw-mb-0");
    map_container.appendChild(better_map_element);

    let box = document.createElement("div");
    box.classList.add("c-inline-map","c-inline-map--product","tw-mb-6","xl:tw-mb-12");
    box.appendChild(map_container);

    document.getElementsByClassName('c-inline-map')[0].parentElement.appendChild(info);
    document.getElementsByClassName('c-inline-map')[0].parentElement.appendChild(box);

    //let better_map = L.map('better_map').setView([48,0], 5);

    let better_map = L.map('better_map').setView([48, 0], 5);
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(better_map);

    // Drawing regions to the map
    var regions = kmtBoot.getProps().packages.models.map(p => p.attributes.region.id);
    regions.forEach(async (id) => {
        if (id == 9999) return;
        fetch("https://www.komoot.com/product/regions?region="+id,{
            headers: {
                'onlyprops': 'true',
            }
        }).then(r => r.json()).then(json => {
            json.regions[0].geometry.forEach((p) => {
                let region = [];
                p.forEach((i) => {
                    region.push([i.lat, i.lng]);
                });
                L.polygon(region).addTo(better_map);
            });
        });
    });

}


function check_for_kmt(){
    if(kmtBoot.getProps().packages.models != undefined){
        if(typeof L == 'undefined'){
            load_leaflet(komoot_regions);
            // komoot_regions() called as callback from leaflet
        }else{
            komoot_regions();
        }
    }else{
        setTimeout(check_for_kmt,200);
    }
}
check_for_kmt();

