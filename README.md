# Adventure Planner

A chrome extension to load the route of your next trip on different webpages.


Implements https://github.com/cahz/komoot-unlocked-regions

## Plans

The extension should be able to load a route from komoot,others(?) and show it on different websites


Planned websites to support:
- 1NITETENT
- iOverlander
- mundraub
- Couchsurfing
- Warm Showers 
- some kind of weather map (https://openweathermap.org/weathermap)
- ....



## Individual page scripts

### 1NITETENT

Add this js:
```
let gpx = 'planned.gpx'; // URL to your GPX file or the GPX itself

// Load gpx for leaflet
jQuery.ajax({
    url: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet-gpx/1.5.1/gpx.min.js',
    dataType: 'script',
    async: true,
    success: () => {
        // draw gpx to map
        let optns = {
            async: true,
            marker_options: {
                startIconUrl: false,
                endIconUrl: false
                }
        }
        new L.GPX(gpx, optns).on('loaded', function(e) {
            map.fitBounds(e.target.getBounds());
        }).addTo(map);
    }
});
```


### iOverlander

Add this js:
```
let gpx = 'planned.gpx'; // URL to your GPX file or the GPX itself

// Load gpx for leaflet
jQuery.ajax({
    url: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet-gpx/1.5.1/gpx.min.js',
    dataType: 'script',
    async: true,
    success: () => {
        // draw gpx to map
        let optns = {
            async: true,
            marker_options: {
                startIconUrl: false,
                endIconUrl: false
                }
        }
        new L.GPX(gpx, optns).on('loaded', function(e) {
            gMap.fitBounds(e.target.getBounds());
        }).addTo(gMap);
    }
});
```

### mundraub

Add this js:
```
let gpx = 'planned.gpx'; // URL to your GPX file or the GPX itself

// Load gpx for leaflet
jQuery.ajax({
    url: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet-gpx/1.5.1/gpx.min.js',
    dataType: 'script',
    async: true,
    success: () => {
        // draw gpx to map
        let optns = {
            async: true,
            marker_options: {
                startIconUrl: false,
                endIconUrl: false
                }
        }
        new L.GPX(gpx, optns).on('loaded', function(e) {
            drupalSettings.mundraub_map.map.fitBounds(e.target.getBounds());
        }).addTo(drupalSettings.mundraub_map.map);
    }
});
```

