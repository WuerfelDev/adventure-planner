// https://stackoverflow.com/a/61226119
const blobToBase64 = blob => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    return new Promise(resolve => {
        reader.onloadend = () => {
            resolve(reader.result);
        };
    });
};

function show(elem){
    document.getElementById(elem).classList.remove("hidden");
}

function hide(elem){
    document.getElementById(elem).classList.add("hidden");
}

async function import_gpx(url){
    show('importing');
    fetch(url).then(r => r.blob()).then(blob => {

        let gpx = blobToBase64(blob).then(b => {
            chrome.storage.local.set({'gpx': b});
            return b;
        });

        const formData = new FormData();
        formData.append('uploaded_file_1',blob);
        formData.append('format','png');
        formData.append('width','350');
        formData.append('height','150');
        formData.append('margin','20');
        formData.append('bg_opacity','70');
        formData.append('trk_name','0');
        formData.append('legend_placement','none');
        let preview = fetch('https://www.gpsvisualizer.com/map?output', {
            method: 'POST',
            body: formData
        }).then(res => res.text()).then(data => {
            let re_png = /download\/[\d-]+-map\.png/g;
            return fetch("https://www.gpsvisualizer.com/" + data.match( re_png )[0])
                .then(im => im.blob())
                .then(blobToBase64)
                .then(data_img => { 
                    chrome.storage.local.set({'preview': data_img});
                    return data_img;
                });
        });
        

        Promise.all([gpx,preview]).then(v => load_route(v[0],v[1]));

    });
}

function load_route(gpx, preview){
    document.getElementById('preview').src = preview;
    hide('no-route');
    hide('importing');
    show('route');
}


chrome.storage.local.get(['gpx','preview'], function(result) {
    if (result.gpx && result.preview){
        load_route(result.gpx,result.preview);
    }else{
        show('no-route');
    }
}); 



chrome.tabs.query({active: true}, tabs => {
    let url = tabs[0].url;
    let re_komoot = /^https:\/\/(?:www\.)?komoot\.com\/tour\/(\d+)/i;
    let komoot_tour = url.match(re_komoot);
    if(komoot_tour && komoot_tour[1]){
        show('import_web');
        document.getElementById('import_web').addEventListener("click", () => {
            import_gpx("https://www.komoot.com/tour/"+komoot_tour[1]+"/download");
        }, false);
    }
});


